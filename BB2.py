import matplotlib.pyplot as plt
from IPython.display import display, clear_output
from random import uniform
from random import randint


# сервера с мощностями
servers = [[] for _ in range(5)]
servers_weigth = [round(uniform(0.1, 1), 1) for _ in range(5)]
servers_work_unused = [0 for _ in range(5)]

#случайная нагрузка
load = [round(uniform(0.1, 0.3), 1) for _ in range(50)] + [round(uniform(0.3, 0.5), 1) for _ in range(50)] + [round(uniform(0.5, 1), 1) for _ in range(50)] + [round(uniform(0.1, 1), 1) for _ in range(50)]

current_server=0

def round_robin_balance(request):
    global current_server
    current_server %= len(servers)
    servers[current_server].append(request)
    current_server += 1




def calculate_servers_load():
    for i, server in enumerate(servers):
        if server != []:
            servers_work_unused[i] += servers_weigth[i]
        while True:
            if server != [] and server[-1] <= servers_work_unused[i]:
                servers_work_unused[i] -= server.pop(-1)
            else:
                break 

counter=1
#График
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

for x in range(0, 300):
    ax.cla()
    plt.title(str(x/2) + f" сек / Долг \nМощности серверов - {' '.join([str(x) for x in servers_weigth])}")

    if x % 10==1 and counter==1:
        for i in range(25):
            round_robin_balance(load[randint(0, 49)])
        plt.title(str(x/2) + " сек / Долг (Легко)")
        counter+=1
    elif x % 10==1 and counter==2:
        for i in range(25):
            round_robin_balance(load[randint(51, 99)])
        plt.title(str(x/2) + " сек / Долг (Средне)")
        counter+=1
        
    elif x % 10==1 and counter==3:
        for i in range(25):
            round_robin_balance(load[randint(101, 149)])
        plt.title(str(x/2) + " сек / Долг (Высокие)")
        counter+=1
        
    elif x % 10==1 and counter==4:
        for i in range(25):
            round_robin_balance(load[randint(151, 199)])
        plt.title(str(x/2) + " сек / Долг (случайные)")
        counter=1
        
    else: 
        calculate_servers_load()
    
    plt.bar(list(range(1, 6)), [sum(server) for server in servers], width=1, bottom=None, align='center')
    display(fig)
    clear_output(wait=True)
    plt.pause(0.5)
